class datorhandbok (
  $mysql_password = undef, # hiera
) {
  ensure_packages([
    'apache2',
    'default-mysql-server',
    'mediawiki',
    'imagemagick',
    ],
    { ensure => installed, })

  service { 'apache2':
    ensure  => running,
    enable  => true,
    require => Package['apache2'],
  }

  service { 'mysql':
    ensure  => running,
    enable  => true,
    require => Package['default-mysql-server'],
  }


  #  ┌───────────────────────────────┤ Configuring mariadb-server-10.1 ├───────────────────────────────┐
  #  │                                                                                                 │
  #  │ Important note for NIS/YP users                                                                 │
  #  │                                                                                                 │
  #  │ Using MariaDB under NIS/YP requires a mysql user account to be added on the local system with:  │
  #  │                                                                                                 │
  #  │  adduser --system --group --home /var/lib/mysql mysql                                           │
  #  │                                                                                                 │
  #  │                                                                                                 │
  #  │ You should also check the permissions and ownership of the /var/lib/mysql directory:            │
  #  │                                                                                                 │
  #  │  /var/lib/mysql: drwxr-xr-x   mysql    mysql                                                    │
  #  │                                                                                                 │
  #  │                                             <Ok>                                                │
  #  │                                                                                                 │
  #  └─────────────────────────────────────────────────────────────────────────────────────────────────┘
  # Mariadb gave the following notice on upgrade. Might as well
  # explicitly specifify it here

  group { 'mysql':
    ensure => present,
    system => true,
  }

  user { 'mysql':
    ensure => present,
    system => true,
    gid    => 'mysql',
    home   => '/var/lib/mysql',
  }

  file { '/var/lib/mysql':
    ensure => directory,
    owner  => mysql,
    group  => mysql,
    mode   => '0755',
  }

  ensure_packages (['logrotate'],
    { ensure => installed })

  file { '/etc/logrotate.d/datorhandbok-mysql':
    ensure => file,
    source => 'puppet:///modules/datorhandbok/logrotate',
  }

  file { '/var/lib/mysql-dump/run.sh':
    ensure => file,
    mode   => '0755',
    source => 'puppet:///modules/datorhandbok/mysql-dump.sh',
  }

  cron { 'backup-mysql':
    command => '/var/lib/mysql-dump/run.sh',
    user    => 'root',
    hour    => 3,
    minute  => 11;

  }

  file { '/etc/apache2/mods-enabled/rewrite.load':
      ensure  => symlink,
      target  => '/etc/apache2/mods-available/rewrite.load',
      owner   => root,
      group   => root,
      notify  => Service['apache2'],
      require => Package['apache2'],
  }

  file { '/etc/apache2/mods-enabled/ssl.load':
      ensure  => symlink,
      target  => '/etc/apache2/mods-available/ssl.load',
      owner   => root,
      group   => root,
      notify  => Service['apache2'],
      require => Package['apache2'],
  }

  class { '::letsencrypt':
    email => 'root@lysator.liu.se',
  }

  ensure_packages ([
    'python3-certbot-apache',
    ],
    { ensure => installed })

  letsencrypt::certonly { $facts['networking']['fqdn']:
    domains            => [ $facts['networking']['fqdn'] ],
    manage_cron        => true,
    cron_hour          => [2, 14],
    cron_minute        => 13,
    plugin             => 'apache',
    additional_args    => [ '--quiet', ],
    post_hook_commands => [ 'systemctl reload apache2' ],
  }

  file { '/etc/apache2/sites-available/datorhandbok.lysator.liu.se.conf':
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0444',
      notify  => Service['apache2'],
      require => Package['apache2'],
      content => epp('datorhandbok/datorhandbok.lysator.liu.se',
        {
          # 'sslcertificatefile'      => '/etc/ssl/certs/datorhandbok.lysator.liu.se/datorhandbok.lysator.liu.se.pem',
          # 'sslcertificatekeyfile'   => '/etc/ssl/certs/datorhandbok.lysator.liu.se/datorhandbok.lysator.liu.se.key',
          # 'sslcertificatechainfile' => '/etc/ssl/certs/DigiCertCA.crt',
          'sslcertificatefile'      => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchain.pem",
          'sslcertificatekeyfile'   => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/privkey.pem",
          'sslcertificatechainfile' => "/etc/letsencrypt/live/${facts['networking']['fqdn']}/fullchain.pem",
        }),
  }

  file { '/etc/apache2/sites-enabled/datorhandbok.lysator.liu.se.conf':
    ensure  => symlink,
    target  => '/etc/apache2/sites-available/datorhandbok.lysator.liu.se.conf',
    owner   => root,
    group   => root,
    notify  => Service['apache2'],
    require => File['/etc/apache2/sites-available/datorhandbok.lysator.liu.se.conf'],
  }

  file { '/etc/apache2/sites-enabled/000-default':
    ensure => absent,
    notify => Service['apache2'],
  }

  concat { '/etc/mediawiki/LocalSettings.php':
    ensure  => present,
    owner   => 'root',
    group   => 'www-data',
    mode    => '0440',
    require => Package['mediawiki'],
  }

  concat::fragment { 'Mediawiki Main config':
    target  => '/etc/mediawiki/LocalSettings.php',
    order   => '00',
    content => epp('datorhandbok/LocalSettings.php',
      # Wrapping $mysql_password in Sensitive() would prevent it from
      # showing up in logs, but the file then contained
      # "Sensitive [value redacted]"...
      { 'mysql_password' => $mysql_password }),
  }

  file { '/var/lib/mediawiki/skins/lyslogo-liten.png':
    ensure  => file,
    owner   => www-data,
    group   => www-data,
    source  => 'puppet:///modules/datorhandbok/lyslogo-liten.png',
    require => Package['mediawiki'],
  }

  file { '/var/lib/mediawiki/favicon.ico':
    ensure  => file,
    owner   => root,
    group   => root,
    source  => 'puppet:///modules/datorhandbok/favicon.ico',
    require => Package['mediawiki'],
  }

  # Set in CASAuthSettings as redirect location on failed login
  file { '/var/lib/mediawiki/login-error.php':
    ensure  => file,
    source  => 'puppet:///modules/datorhandbok/login-error.php',
    require => Package['mediawiki'],
  }

  file { '/etc/php/7.4/apache2/conf.d/20-curl.ini':
    ensure => link,
    target => '/etc/php/7.3/mods-available/curl.ini',
  }


  # mods-enabled/php* might need to be removed

  file { [
    '/etc/apache2/mods-enabled/php7.4.conf',
    '/etc/apache2/mods-enabled/php7.4.load'
  ]:
      ensure => absent,
  }

  file { '/etc/apache2/mods-enabled/php8.2.conf':
    ensure => link,
    target => '../mods-available/php8.2.conf',
  }

  file { '/etc/apache2/mods-enabled/php8.2.load':
    ensure => link,
    target => '../mods-available/php8.2.load',
  }

  $dpl_version = '3.5.2'

  datorhandbok::extension { 'DynamicPageList3':
    tar_name => "${dpl_version}.tar.gz",
    dir_name => "DynamicPageList3-${dpl_version}",
    url      => "https://github.com/Universal-Omega/DynamicPageList3/archive/refs/tags/${dpl_version}.tar.gz",
  }


  # PluggableAuth might require composer to be run after it's
  # installed. Try running the below code if it doesn't work on a
  # fresh install.

  # ensure_packages(['composer'], { ensure => installed })

  # file { '/var/lib/mediawiki/composer.local.json':
  #   ensure  => file,
  #   content => @(EOF)
  #   {
  #     "extra": {
  #       "merge-plugin": {
  #         "include": [
  #           "extensions/OpenIDConnect/composer.json"
  #         ]
  #       }
  #     }
  #   }
  #   | EOF
  # } ~> exec { 'composer update --no-interactive':
  #   cwd         => '/var/lib/mediawiki',
  #   refreshonly => true,
  #   path        => ['/usr/bin', '/bin'],
  # }

  # Some of these links use Mediawikis plugin-repo, which explicitly
  # states to NOT bookmark their links. They however don't seem to
  # provide simple stable links. Something along the lines of
  # ```python3
  # >>> import requests
  # >>> r = requests.get('https://www.mediawiki.org/wiki/Special:ExtensionDistributor?extdistname=PluggableAuth&extdistversion=REL1_35')
  # >>> r.headers['refresh']
  # '5;url=https://extdist.wmflabs.org/dist/extensions/PluggableAuth-REL1_35-e5976c0.tar.gz'
  # ```
  # Could however be done

  datorhandbok::extension { 'PluggableAuth':
    tar_name => 'PluggableAuth-REL1_42-f0a83a8.tar.gz',
    url      => 'https://extdist.wmflabs.org/dist/extensions/PluggableAuth-REL1_42-f0a83a8.tar.gz'
  }

  datorhandbok::extension { 'OpenIDConnect':
    tar_name => 'OpenIDConnect-REL1_42-957026f.tar.gz',
    url      => 'https://extdist.wmflabs.org/dist/extensions/OpenIDConnect-REL1_42-957026f.tar.gz',
  }

  $openid_config = @(EOF)
    # Should contain a definition for $openid_secret, until I have
    # figured out how to push secrets with hiera.
    require_once( "secret.php" );

    # uses existing account if username already exists
    $wgOpenIDConnect_MigrateUsersByUserName = true;

    $wgPluggableAuth_Config[] = [
      'plugin' => 'OpenIDConnect',
      'data' => [
        'providerURL'  => 'https://login.lysator.liu.se/realms/Lysator',
        'clientID'     => 'datorhandbok',
        'clientsecret' => $openid_secret,
        'scope'        => ['openid', 'profile', 'email']
      ]
    ];
    | EOF

  concat::fragment { 'Mediawiki OpenID config':
    target  => '/etc/mediawiki/LocalSettings.php',
    content => $openid_config,
  }

  file { '/etc/mediawiki-extensions/extensions-enabled/RSSReader.php':
    ensure  => symlink,
    target  => '/etc/mediawiki-extensions/extensions-available/RSSReader.php',
    require => Package['mediawiki'],
  }
}
