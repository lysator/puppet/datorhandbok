# Sets up required environment on the system for extracting mediawiki
# extensions. Also provides a global variable for where (on disk) extensions
# should be managed.
class datorhandbok::extension_init (
  $ext_dir = '/opt/mediawiki-extensions',
) {
  file { $ext_dir:
    ensure => directory,
  }
}
