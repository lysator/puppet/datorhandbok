define datorhandbok::extension (
  String $tar_name,
  String $url,
  String $extension_name = $name,
  String $dir_name = $extension_name,
) {

  require ::datorhandbok::extension_init

  $d = $::datorhandbok::extension_init::ext_dir

  file { "${d}/${extension_name}":
    ensure => directory,
  }

  # In most cases this creates a doubly nested directory of the
  # plugins name (e.g. $d/ExtensionName/ExtensionName). This is
  # however per design, since the inner dir could have the missfortune
  # of being called "master" (or similar), depending on it packaging.
  archive { "${d}/${extension_name}/${tar_name}":
    ensure       => present,
    url          => $url,
    creates      => "${d}/${extension_name}/${dir_name}",
    extract      => true,
    extract_path => "${d}/${extension_name}",
  }

  file { "/var/lib/mediawiki/extensions/${extension_name}":
    ensure => link,
    target => "${d}/${extension_name}/${dir_name}",
  }

  concat::fragment { "LocalSettings set ${extension_name} ":
    target  => '/etc/mediawiki/LocalSettings.php',
    content => "wfLoadExtension('${extension_name}');\n",
    order   => '99',
  }
}
