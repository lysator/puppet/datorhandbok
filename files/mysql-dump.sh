#!/bin/bash

# fail faster
set -e

TMPFILE=$(/bin/mktemp /var/tmp/fulldump.sql.XXX)
/usr/bin/mysqldump --all-databases --events > "$TMPFILE"
mv "$TMPFILE" /var/lib/mysql-dump/fulldump.sql
